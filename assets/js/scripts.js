var $ = jQuery.noConflict();

$(document).ready(function() {


    //FANCYBOX
    $("[data-fancybox]").fancybox({
        toolbar: true,
        loop: true,
        buttons : [
            'close'
        ],
    });


    //IMAGE LOADER
    function imageLoaderInit() {
        // IMAGE LOADER
        var src = 'lowsrc';

        $('.js-img-bg[data-src^="http"]').each(function () {
            var spnr = Hello.spinner();
            spnr.spin(this);
        });

        if ((window.devicePixelRatio == 1 && window.innerWidth > 1024  ) || ( window.devicePixelRatio > 1 && window.innerWidth > 640)) {
            src = 'src';
        }
        var $loadimages = $('.js-img-bg');
        $loadimages.imageloader({
            dataattr: src,
            background: true,
            each: function (elm) {
                $(elm).find('.spinnercss').fadeOut('fast');
            },
            callback: function (elm) {
                //var supportsBackgroundBlendMode = window.getComputedStyle(document.body).backgroundBlendMode;
                //if(typeof supportsBackgroundBlendMode == 'undefined') {
                //    createBlendedBackgrounds();
                //}
            },
            eacherror: function (elm) {
                // fallback for background removing!?
                var originalSrc = $(elm).data('src');
                $(elm).css('background-image', 'url(' + originalSrc + ')');
                $(elm).removeAttr(['data-', 'src'].join(''));
                $(elm).find('.spinnercss').fadeOut('fast');
            },
            timeout: 20000
        });
    }
    imageLoaderInit();

    

});
/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
  jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
      jQuery(this).wrap("<div class='widescreen flex-video'/>");
    } else {
      jQuery(this).wrap("<div class='flex-video'/>");
    }
  });

});

jQuery(document).foundation();