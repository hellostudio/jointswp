var $ = jQuery.noConflict();

//ROZWIJANE BOKSY
var _isMobile = function () {
    return /mobile|iphone|ipad|ipod|android|iemobile|opera mobi|opera mini/.test(window.navigator.userAgent.toLowerCase());
};

// FIX for touchscroll and touchend
var _dragging = false;
$('body').on('touchmove', function () {
    _dragging = true;
});
$('body').on('touchstart', function () {
    _dragging = false;
});


$(document).ready(function () {

    var wrapper = $('.js-boxes-wrap');

    wrapper.each(function () {
        var wrapper = $(this);
        var boxes = wrapper.find('.js-boxes-item');

        if (boxes.length > 0) {

            var boxSize = Math.floor(boxes.eq(0)[0].getBoundingClientRect().width),
            //boxSize = boxes.eq(0).outerWidth();
                w = wrapper.innerWidth(),
                breakat = Math.floor(w / boxSize),
                extraItem,
                $padderTpl = $('<span/>').addClass('padder');


            // console.log(breakat);

            var last_per_row = boxes.filter(':nth-child(' + breakat + 'n)');
            var lastElement;

            // add Extras on page load
            if (boxes.length % breakat != 0) {
                lastElement = $(boxes.filter(':last-child'));
            }

            last_per_row.addClass('extra-after');
            if (lastElement != undefined && lastElement.length > 0) {
                lastElement.addClass('extra-after');
            }

            $('.extra-after').after($padderTpl);

            $(window).resize(function () {
                if ($(window).innerWidth() > 1024) {
                    $('.padder').remove();
                }
            });
            $(window).resize($.debounce(250, function () {
                var $window = $(window);

                boxes.removeClass('first');

                // dont resize on iphone etc. (hiding browser bar)
                if ($window.innerWidth() > 1024) {
                    // console.log('asd');
                    // clear extra for new items
                    boxes.removeClass('extra-after');
                    boxes.removeClass('opened');
                    boxes.removeClass('box-opened');
                    $('.show-arrow').removeClass('show-arrow');

                    // hide extra info on resize
                    $('.js-boxes-expanded').css({
                        height: 0
                    });
                    // remove row padders
                    $('.padder').remove();

                    // calculate stuff
                    //boxSize = boxes.eq(0).outerWidth();
                    boxSize = Math.floor(boxes.eq(0)[0].getBoundingClientRect().width);
                    w = wrapper.innerWidth();
                    breakat = Math.floor(w / boxSize); // this calculates how many items fit in a row

                    // set new last per row items
                    last_per_row = boxes.filter(':nth-child(' + breakat + 'n)'); // target the last element of each row
                    lastElement = undefined;

                    if (boxes.length % breakat != 0) {
                        lastElement = $(boxes.filter(':last-child'));
                    }

                    if (lastElement != undefined && lastElement.length > 0) {
                        lastElement.addClass('extra-after');
                    }

                    // add extra class for items
                    last_per_row.addClass('extra-after');

                    $('.extra-after').after($padderTpl);
                }
            }));

            // Listen for orientation changes
            window.addEventListener("orientationchange", function () {
                // console.log('orientation change');
                // clear extra for new items
                boxes.removeClass('extra-after');
                boxes.removeClass('opened');
                boxes.removeClass('box-opened');
                $('.show-arrow').removeClass('show-arrow');

                $('.js-boxes-open').removeClass('box-opened');

                // hide extra info on resize
                $('.js-boxes-expanded').css({
                    height: 0
                });
                // remove row padders
                $('.padder').remove();

                // calculate stuff
                //boxSize = boxes.eq(0).outerWidth();
                boxSize = Math.floor(boxes.eq(0)[0].getBoundingClientRect().width);
                w = wrapper.innerWidth();
                breakat = Math.floor(w / boxSize); // this calculates how many items fit in a row

                // set new last per row items
                last_per_row = boxes.filter(':nth-child(' + breakat + 'n)'); // target the last element of each row
                lastElement = undefined;

                if (boxes.length % breakat != 0) {
                    lastElement = $(boxes.filter(':last-child'));
                }

                if (lastElement != undefined && lastElement.length > 0) {
                    lastElement.addClass('extra-after');
                }

                // add extra class for items
                last_per_row.addClass('extra-after');

                $('.extra-after').after($padderTpl);

            }, false);

            $(window).on('changed.zf.mediaquery', function (event, newSize, oldSize) {
                // newSize is the name of the now-current breakpoint, oldSize is the previous breakpoint
                // clear extra for new items
                boxes.removeClass('extra-after');
                boxes.removeClass('opened');
                boxes.removeClass('box-opened');
                $('.show-arrow').removeClass('show-arrow');

                $('.js-boxes-open').removeClass('box-opened');

                // hide extra info on resize
                $('.js-boxes-expanded').css({
                    height: 0
                });
                // remove row padders
                $('.padder').remove();

                // calculate stuff
                //boxSize = boxes.eq(0).outerWidth();
                boxSize = Math.floor(boxes.eq(0)[0].getBoundingClientRect().width);
                w = wrapper.innerWidth();
                breakat = Math.floor(w / boxSize); // this calculates how many items fit in a row

                // set new last per row items
                last_per_row = boxes.filter(':nth-child(' + breakat + 'n)'); // target the last element of each row
                lastElement = undefined;

                if (boxes.length % breakat != 0) {
                    lastElement = $(boxes.filter(':last-child'));
                }

                if (lastElement != undefined && lastElement.length > 0) {
                    lastElement.addClass('extra-after');
                }

                // add extra class for items
                last_per_row.addClass('extra-after');

                $('.extra-after').after($padderTpl);
                $('.padder').css({
                    paddingBottom: 0
                });
            });

        }
    });

    var _productAnimating = false;

    $(document).on('click touchend', '.js-boxes-open', function (ev) {


        ev.preventDefault();
        ev.stopPropagation();

        if (_productAnimating) {
            return;
        }

        var $jsProductWrapper = $(this);

        if (_dragging) {
            return;
        }

        // if this is first click delay is 0
        var delay = 0;

        // if there is any open product close it first
        var opened = $('.js-boxes-item.opened');
        var onlyClose = $(this).parent().hasClass('opened');


        var $clicked = $(this).parent();


        if (opened.length > 0) {
            _productAnimating = true;
            // set delay to open after the hiding is done
            delay = 1000;

            $('.js-boxes-item.opened').each(function (i, el) {
                $(el).find('.js-boxes-expanded').animate({
                    height: 0
                }, {
                    duration: 1000,
                    done: function () {
                        if ($clicked.not('.show-arrow')) {
                            $clicked.addClass('show-arrow');
                            // console.log(true);
                        }
                        $(el).removeClass('show-arrow');
                    }
                });
                $('.padder').animate({
                    paddingBottom: 0,
                }, {
                    duration: 1000
                });

            });

            // remove all opened classes from products
            $('.js-boxes-item.opened').removeClass('opened').removeClass('box-opened');
            $('.js-boxes-open').removeClass('box-opened');

        } else {
            $clicked.addClass('show-arrow');
        }

        var $productLink = $(this),
            $productWrapper = $productLink.parent(),
            $extra = $(this).next('.js-boxes-expanded');


        // if the parent of link is the last use him as the extraItem
        if ($productLink.parent().hasClass('extra-after')) {
            extraItem = $productLink.parent();
        } else {
            extraItem = $productLink.parent().nextAll('.extra-after:first');
        }

        if (!onlyClose) {
            _productAnimating = true;
            $jsProductWrapper.parent('.js-boxes-item').addClass('box-opened');

            // animate the Extra Info in a product with the padder at the same time
            $extra.delay(delay).animate({
                height: $extra.get(0).scrollHeight
            }, {
                duration: 1000,
                step: function (now) {
                    extraItem.next('.padder').eq(0).css({
                        paddingBottom: now + 'px',
                    });
                },
                done: function () {

                    $productWrapper.addClass('opened');
                    $(window).scrollTo($productLink, 400, {
                        offset: -170
                    });
                    _productAnimating = false;
                }
            });
        } else {
            _productAnimating = false;

        }

    });

});