var Hello = window.Hello || {};

// DOM ready
(function ($) {
    Hello.spinner = function (options) {

        var spinnerOpts,
            spinner;

        spinnerOpts = $.extend({
                lines: 8, // The number of lines to draw
                length: 6, // The length of each line
                width: 6, // The line thickness
                radius: 12, // The radius of the inner circle
                corners: 0.5, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#c0181a', // #rgb or #rrggbb or array of colors
                speed: 2, // Rounds per second
                trail: 59, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: true, // Whether to use hardware acceleration
                className: 'spinnercss', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent
                left: '50%' // Left position relative to parent
            },
            options
        );

        spinner = new Spinner(spinnerOpts);

        return spinner;
    };


})(jQuery);