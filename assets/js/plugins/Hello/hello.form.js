var Hello = window.Hello || {};

// DOM ready
(function ($) {

    Hello.form = function(options) {
        var Form = {
            form: 'form',
            messageContainer: '.row.message',
            timeout: 4000,
            parsley: false,
            parsleyLocale: 'en',
            //url: $(this.form).attr('action'),
            messages: {
                tpl: $('<h1></h1>'),
                success: 'Thanks. Your message has been sent.',
                error: 'Error. Please try again.',
            },
            _getMessage: function(type) {
                if ( this.messages.type != 'undefined' ) {
                    return $(this.messages.tpl).html(this.messages.type);
                }
            },

            _showMessage: function(type) {
                $(this.messageContainer).html(this._getMessage(type));
                $(this.form).removeClass('active');
            },

            _hideMessage: function() {
                $(this.messageContainer).html('');
                $(this.form).find('[type="submit"]').attr('disabled', false);
            },

            _submit: function() {
                e.preventDefault();



            },

            _doAjax: function() {

            },

            init: function() {
                console.log('form running');
            }


        };

        $.extend(Form, options);

        Form.init();


        return Form;
    }

})(jQuery);