var Hello = window.Hello || {};

// DOM ready
(function ($) {
    Hello.currentSection = function (options) {
        var $window = $(window),

            defaults = {
                sections: 'section',
                delta: (window.innerWidth < 640 ) ? 10 : 200,
                activeClassStr: 'active',
                menuItemClass: '.submenu-item',
                defaultCurrent: '#home',
                preserveOnEmpty: false
            },

            config = $.extend({}, defaults, options),
            currentSection = config.defaultCurrent;


        var _watcher = function () {
            var windowTop = window.top.pageYOffset;
            var sections = $(config.sections);
            var delta = config.delta;
            var oldSection = currentSection;

            sections.each(function (index, section) {

                var offsetTop = $(section).offset().top;
                var offsetBottom = $(section).offset().top + $(section).outerHeight();

                // console.log(offsetTop);
                // console.log(offsetBottom);

                if (windowTop + delta > offsetTop && windowTop < offsetBottom - delta) {
                    currentSection = '#' + $(section).attr('id');
                    if (oldSection != currentSection /*&& $('.menu-item a[href="#' + $(section).attr('id') + '"]').length > 0*/) {
                        $(config.menuItemClass).removeClass('active');
                    }
                    $('a[href="#' + $(section).attr('id') + '"]', $(config.menuItemClass)).parent().addClass('active');

                    $('.slicknav_nav li').removeClass('active');
                    $('.slicknav_nav li a[href="#' + $(section).attr('id') + '"]').parent().addClass('active');
                }
            });
        }

        if (typeof $.debounce == 'function') {
            $(window).on('scroll', $.debounce(100, _watcher));
        } else {
            $(window).on('scroll', _watcher);

        }


        return this;
    };

})(jQuery);