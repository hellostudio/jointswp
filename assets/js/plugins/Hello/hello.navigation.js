var Hello = window.Hello || {};

// DOM ready
(function ($) {

    Hello.navigationHiding = function(options) {
        var navigationHiding = {
            initialize: false,
            menu: '.menu-desktop',
            mobile: false,
            mobileBreakpoint: 768,
            offsetBreakpoint: false,
            offset: function(customOffset) {
                var defaultOffset = $(window).innerHeight() * .6; // 60% of screen
                return (customOffset) ? customOffset : defaultOffset;
            },
            _watcher: function() {
                if (window.top.pageYOffset >= HelloConfig.navigationHiding.offset()) {
                    $(HelloConfig.navigationHiding.menu).stop().fadeIn();
                } else {
                    $(HelloConfig.navigationHiding.menu).stop().fadeOut('fast');
                }
            },
            _onScroll: function() {
                // console.log('scrolling');
                HelloConfig.navigationHiding._watcher();
            },
            init: function() {
                console.log('init');
                if (window.innerWidth > HelloConfig.navigationHiding.mobileBreakpoint && HelloConfig.navigationHiding.mobile ) {
                    // initial hiding
                    $(HelloConfig.navigationHiding.menu).hide();

                    // initial run on page load
                    HelloConfig.navigationHiding._watcher();

                    // check if debounce present and run
                }

                if (typeof $.debounce == 'function' ) {
                    $(window).on('scroll', $.debounce(100, HelloConfig.navigationHiding._onScroll));
                } else {
                    $(window).on('scroll', HelloConfig.navigationHiding._onScroll);
                }
            }
        };

        if ( options != 'undefined' ) {
            navigationHiding = $.extend({}, navigationHiding, options);
        }


        return navigationHiding;
    }

})(jQuery);