var $ = jQuery.noConflict();

$(document).ready(function() {


    //FANCYBOX
    $("[data-fancybox]").fancybox({
        toolbar: true,
        loop: true,
        buttons : [
            'close'
        ],
    });


    //IMAGE LOADER
    function imageLoaderInit() {
        // IMAGE LOADER
        var src = 'lowsrc';

        $('.js-img-bg[data-src^="http"]').each(function () {
            var spnr = Hello.spinner();
            spnr.spin(this);
        });

        if ((window.devicePixelRatio == 1 && window.innerWidth > 1024  ) || ( window.devicePixelRatio > 1 && window.innerWidth > 640)) {
            src = 'src';
        }
        var $loadimages = $('.js-img-bg');
        $loadimages.imageloader({
            dataattr: src,
            background: true,
            each: function (elm) {
                $(elm).find('.spinnercss').fadeOut('fast');
            },
            callback: function (elm) {
                //var supportsBackgroundBlendMode = window.getComputedStyle(document.body).backgroundBlendMode;
                //if(typeof supportsBackgroundBlendMode == 'undefined') {
                //    createBlendedBackgrounds();
                //}
            },
            eacherror: function (elm) {
                // fallback for background removing!?
                var originalSrc = $(elm).data('src');
                $(elm).css('background-image', 'url(' + originalSrc + ')');
                $(elm).removeAttr(['data-', 'src'].join(''));
                $(elm).find('.spinnercss').fadeOut('fast');
            },
            timeout: 20000
        });
    }
    imageLoaderInit();

    

});