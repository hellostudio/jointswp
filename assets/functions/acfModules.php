<?php


namespace Hellostudio;


class acfModules {

    public static $instance = null;

    public static function load() {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public $modules = array();

    function init() {
        add_action('init', array($this, 'addFilters'));
    }

    public function getModules() {
        $base_path = get_stylesheet_directory();

        $modules_path = $base_path . '/parts/acf';

        $acf_paths = glob($modules_path . '/**/*.json');

        return $acf_paths;
    }

    public function add_acfPaths($paths) {
        $acf_files = $this->getModules();


        foreach ( $acf_files as $k => $path ) {
            $paths[] = dirname($path);
        }

        return $paths;
    }

    public function addFilters() {
        add_filter('acf/settings/load_json', 'add_module_paths', 10);
    }
}