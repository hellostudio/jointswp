<?php

final class Helpers
{

    /**
     * @param int $id
     * @param string $size
     * @return string
     */
    public static function imageUrlFromId($id, $size = 'medium') {
        $imageSrc = wp_get_attachment_image_src($id, $size);
        if (!empty($imageSrc[0])) {
            return $imageSrc[0];
        }

        return '';
    }


    /**
     * @param int $post
     * @return array
     */
    public static function postCategoriesFromId($post = 0) {
        $taxonomies = wp_get_post_terms($post, 'category');
        if ($taxonomies instanceof \WP_Error) {
            return array();
        }

        return $taxonomies;
    }


    /**
     * @param int $id
     * @return null|string
     */
    public static function getChildrenFromID($id = 0) {
        /**
         * @var $wpdb \wpdb;
         */
        global $wpdb;

        if ($id == 0) {
            return null;
        }

        $query = $wpdb->prepare("SELECT COUNT(*) as children FROM {$wpdb->posts} as p WHERE p.post_parent = %d", $id);
        $results = ($wpdb->get_results($query, ARRAY_A));
        if (empty($results[0]['children'])) {
            return false;
        }
        $count = $results[0]['children'];

        return $count;
    }

    /**
     * @param string $content
     * @param int $length default 50
     * @return bool|mixed|string
     */
    public static function shortenText($content = '', $length = 50) {
        // if no content, fail
        if (empty($content)) {
            return false;
        }

        //strip shortcodes & tags
        $content = strip_shortcodes($content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = strip_tags($content);
        $excerpt_length = $length;

        $words = explode(' ', $content, $excerpt_length + 1);
        $totalCount = count($words);
        //if the content is longer than the limit
        if (count($words) > $excerpt_length) {
            array_pop($words);
        }

        $content = implode(' ', $words);
        $content = ($totalCount > $excerpt_length) ? $content . '...' : $content;

        // Make sure to return the content
        return $content;
    }

    /**
     * @param bool $postType
     * @return array|null|object
     */
    public static function getYearsFromPostType($postType = false) {

        if (empty($postType)) {
            return array();
        }

        global $wpdb;
        $where = $wpdb->prepare("WHERE post_type = %s AND post_status = 'publish'", $postType);
        $query = "SELECT YEAR(post_date) AS `year`, count(ID) as posts FROM $wpdb->posts $where GROUP BY YEAR(post_date) ORDER BY post_date";
        $results = $wpdb->get_results($query);

        if ($results) {
            return $results;
        }

    }

    /**
     * @param bool $postType
     * @return array|null|object
     */
    public static function getYearsFromPostTypeMeta($postType = false) {

        if (empty($postType)) {
            return array();
        }

        global $wpdb;
        $select = "SELECT YEAR(pm.meta_value) AS `year`, COUNT(ID) AS posts FROM $wpdb->posts as p";
        $join = "LEFT JOIN $wpdb->postmeta as pm ON pm.post_id =  p.ID";
        $where = $wpdb->prepare("WHERE post_type = '%s' AND post_status = 'publish' AND meta_key = 'date' ", $postType);
        $query = "$select $join $where GROUP BY YEAR(meta_value) ORDER BY meta_value";

        $results = $wpdb->get_results($query);

        if ($results) {
            return $results;
        }

    }


    /**
     * @param array $data
     * @param string $fieldName
     * @return array
     */
    public static function categoriesFromRepeaterData($data = array(), $fieldName = '') {
        $categories = array();

        if (empty($data) || empty($fieldName)) {
            return array();
        }

        foreach ($data as $k => $item) {
            $categories[] = (Object)array(
                'name' => $item[$fieldName],
                'slug' => sanitize_title($item[$fieldName]),
            );
        }

        return $categories;
    }

    /**
     * @param $url
     * @param bool $autoPlay
     * @return bool|string
     */
    public static function getProviderFromUrl($url, $autoPlay = false) {
        $re = "/(player.|www.)?(vimeo\\.com|youtu(be\\.com|\\.be|be\\.googleapis\\.com))\\/(video\\/|embed\\/|watch\\?v=|v\\/)?([A-Za-z0-9._%-]*)(\\&\\S+)?/";
        $autoString = '';
        if ($autoPlay) {
            $autoString = '?autoplay=1';
        }

        preg_match($re, $url, $matches);

        if (strrpos($matches[2], 'youtu') > -1) {
            $type = 'youtube';
            $src = 'https://www.youtube.com/embed/' . $matches[5] . $autoString;
        } else if (strrpos($matches[2], 'vimeo') > -1) {
            $type = "vimeo";
            $src = 'https://player.vimeo.com/video/' . $matches[5] . $autoString;
        } else {
            return false;
        }

        return $src;
    }


    public static function getYoutubeThumbnailFromURL($url) {
        if (empty($url)) {
            return '';
        }

        parse_str(parse_url($url, PHP_URL_QUERY), $ytarray);
        $id = $ytarray['v'];

        if (!empty($id)) {
            return "http://img.youtube.com/vi/" . $id . "/0.jpg";
        }

        return '';
    }


    public static function getMimeIcon($mime_type) {
        // List of official MIME Types: http://www.iana.org/assignments/media-types/media-types.xhtml
        $icon_classes = array(
            'image'                                                          => get_template_directory_uri() . '/assets/images/download-icons/jpg-icon.png',
            'application/pdf'                                                => get_template_directory_uri() . '/assets/images/download-icons/pdf-icon.png',
            'application/msword'                                             => get_template_directory_uri() . '/assets/images/download-icons/doc-icon.png',
            'application/vnd.ms-word'                                        => get_template_directory_uri() . '/assets/images/download-icons/doc-icon.png',
            'application/vnd.oasis.opendocument.text'                        => get_template_directory_uri() . '/assets/images/download-icons/doc-icon.png',
            'application/vnd.openxmlformats-officedocument.wordprocessingml' => get_template_directory_uri() . '/assets/images/download-icons/doc-icon.png',
            'application/vnd.ms-excel'                                       => get_template_directory_uri() . '/assets/images/download-icons/xls-icon.png',
            'application/vnd.openxmlformats-officedocument.spreadsheetml'    => get_template_directory_uri() . '/assets/images/download-icons/xls-icon.png',
            'application/vnd.oasis.opendocument.spreadsheet'                 => get_template_directory_uri() . '/assets/images/download-icons/xls-icon.png',
            'application/vnd.ms-powerpoint'                                  => get_template_directory_uri() . '/assets/images/download-icons/ppt-icon.png',
            'application/vnd.openxmlformats-officedocument.presentationml'   => get_template_directory_uri() . '/assets/images/download-icons/ppt-icon.png',
            'application/vnd.oasis.opendocument.presentation'                => get_template_directory_uri() . '/assets/images/download-icons/ppt-icon.png',
            'text/plain'                                                     => get_template_directory_uri() . '/assets/images/download-icons/doc-icon.png',
            'application/zip'                                                => get_template_directory_uri() . '/assets/images/download-icons/zip-icon.png',
        );
        foreach ($icon_classes as $text => $icon) {
            if (strpos($mime_type, $text) === 0) {
                return $icon;
            }
        }

        return 'fa-file-o';
    }


    /**
     * @param $page_id
     * @return bool|WP_Post
     */
    public static function getRootPage($page_id) {
        $parent_id = wp_get_post_parent_id($page_id);
        $parent = get_post($parent_id);

        if ($parent_id === false || $parent === null) {
            return false;
        }

        if( $parent->post_parent == 0 ) {
            return $parent;
        } else {
            return self::getRootPage($parent_id);
        }
    }

    /**
     * @param $page_id
     * @return array|false
     */
    public static function getPageChildren($page_id) {
        $pages = get_pages(array(
            'child_of'     => $page_id,
            'parent'       => $page_id,
            'hierarchical' => false,
            'sort_column'  => 'menu_order',

        ));

        return $pages;
    }
    
}