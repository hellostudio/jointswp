/// Font family for button elements.
/// @type Font
$button-font-family: inherit !default;

/// Padding inside buttons.
/// @type List
$button-padding: 0.85em 1em !default;

/// Margin around buttons.
/// @type List
$button-margin: 0 0 $global-margin 0 !default;

/// Default fill for buttons. Can either be `solid` or `hollow`.
/// @type Keyword
$button-fill: solid !default;

/// Default background color for buttons.
/// @type Color
$button-background: $primary-color !default;

/// Background color on hover for buttons.
/// @type Color
$button-background-hover: scale-color($button-background, $alpha: -15%) !default;

/// Font color for buttons.
/// @type List
$button-color: $white !default;

/// Alternative font color for buttons.
/// @type List
$button-color-alt: $black !default;

/// Border radius for buttons, defaulted to global-radius.
/// @type Number
$button-radius: $global-radius !default;

/// Border width for hollow outline buttons
/// @type Number
$button-hollow-border-width: 1px !default;

/// Sizes for buttons.
/// @type Map
$button-sizes: (
  tiny: 0.6rem,
  small: 0.75rem,
  default: 0.9rem,
  large: 1.25rem,
) !default;

/// Coloring classes. A map of classes to output in your CSS, like `.secondary`, `.success`, and so on.
/// @type Map
$button-palette: $foundation-palette !default;

/// opacity for a disabled button.
/// @type List
$button-opacity-disabled: 0.25 !default;

/// Background color lightness on hover for buttons.
/// @type Number
$button-background-hover-lightness: -20% !default;

/// Color lightness on hover for hollow buttons.
/// @type Number
$button-hollow-hover-lightness: -50% !default;

// Internal: flip from margin-right to margin-left for defaults
@if $global-text-direction == 'rtl' {
  $button-margin: 0 0 $global-margin $global-margin !default;
}

/// transitions for buttons.
/// @type List
$button-transition: background-color 0.25s ease-out, color 0.25s ease-out !default;

// TODO: Document button-base() mixin
@mixin button-base {
  @include disable-mouse-outline;
  display: inline-block;
  vertical-align: middle;
  margin: $button-margin;
  font-family: $button-font-family;

  @if (type-of($button-padding) == 'map') {
    @each $size, $padding in $button-padding {
      @include breakpoint($size) {
        padding: $padding;
      }
    }
  }
  @else {
    padding: $button-padding;
  }

  -webkit-appearance: none;
  border: 1px solid transparent;
  border-radius: $button-radius;
  transition: $button-transition;

  font-size: 1rem;
  line-height: 1;
  text-align: center;
  cursor: pointer;
}

/// Expands a button to make it full-width.
/// @param {Boolean} $expand [true] - Set to `true` to enable the expand behavior. Set to `false` to reverse this behavior.
@mixin button-expand($expand: true) {
  @if $expand {
    display: block;
    width: 100%;
    margin-right: 0;
    margin-left: 0;
  }
  @else {
    display: inline-block;
    width: auto;
    margin: $button-margin;
  }
}

/// Sets the visual style of a button.
/// @param {Color} $background [$button-background] - Background color of the button.
/// @param {Color} $background-hover [$button-background-hover] - Background color of the button on hover. Set to `auto` to have the mixin automatically generate a hover color.
/// @param {Color} $color [$button-color] - Text color of the button. Set to `auto` to automatically generate a color based on the background color.
@mixin button-style(
  $background: $button-background,
  $background-hover: $button-background-hover,
  $color: $button-color,
  $background-hover-lightness: $button-background-hover-lightness
) {
  //@if $color == auto {
  //  $color: color-pick-contrast($background, ($button-color, $button-color-alt));
  //}

  @if $background-hover == auto {
    $background-hover: scale-color($background, $alpha: $background-hover-lightness);
  }

  background-color: $background;
  color: $color;

  &:hover, &:focus {
    background-color: $background-hover;
    color: $color;
  }
}

/// Removes background fill on hover and focus for hollow buttons.
@mixin button-hollow {
  &,
  &:hover, &:focus {
    background-color: transparent;
  }

  &.disabled,
  &[disabled] {
    &,
    &:hover, &:focus {
      background-color: transparent;
    }
  }
}

@mixin button-hollow-style(
  $color: $button-background,
  $hover-lightness: $button-hollow-hover-lightness,
  $border-width: $button-hollow-border-width
) {
  $color-hover: scale-color($color, $alpha: $hover-lightness);

  border: $border-width solid $color;
  color: $color;

  &:hover, &:focus {
    border-color: $color-hover;
    color: $color-hover;
    &.disabled,
    &[disabled] {
      border: $border-width solid $color;
      color: $color;
    }
  }
}

/// Adds disabled styles to a button by fading the element, reseting the cursor, and disabling pointer events.
/// @param [Color] $background [$button-background] - Background color of the disabled button.
/// @param [Color] $color [$button-color] - Text color of the disabled button. Set to `auto` to have the mixin automatically generate a color based on the background color.
@mixin button-disabled(
  $background: $button-background,
  $color: $button-color
) {
  //@if $color == auto {
  //  $color: color-pick-contrast($background, ($button-color, $button-color-alt));
  //}

  opacity: $button-opacity-disabled;
  cursor: not-allowed;

  &, &:hover, &:focus {
    background-color: $background;
    color: $color;
  }
}


/// Adds all styles for a button. For more granular control over styles, use the individual button mixins.
/// @param {Boolean} $expand [false] - Set to `true` to make the button full-width.
/// @param {Color} $background [$button-background] - Background color of the button.
/// @param {Color} $background-hover [$button-background-hover] - Background color of the button on hover. Set to `auto` to have the mixin automatically generate a hover color.
/// @param {Color} $color [$button-color] - Text color of the button. Set to `auto` to automatically generate a color based on the background color.
/// @param {Keyword} $style [solid] - Set to `hollow` to create a hollow button. The color defined in `$background` will be used as the primary color of the button.
@mixin button(
  $expand: false,
  $background: $button-background,
  $background-hover: $button-background-hover,
  $color: $button-color,
  $style: $button-fill
) {
  @include button-base;

  @if $style == solid {
    @include button-style($background, $background-hover, $color);
  }
  @else if $style == hollow {
    @include button-hollow;
    @include button-hollow-style($background);
  }

  @if $expand {
    @include button-expand;
  }
}

@mixin c-button {
  .button {
    @include button;

    // Sizes
    @each $size, $value in map-remove($button-sizes, default) {
      &.#{$size} {
        //font-size: $value;
      }
    }

    &.expanded { @include button-expand; }

    // Colors
    @each $name, $color in $button-palette {
      @if $button-fill != hollow {
        &.#{$name} {
          @include button-style($color, auto, auto);
        }
      }
      @else {
        &.#{$name} {
          @include button-hollow-style($color);
        }

        &.#{$name}.dropdown::after {
          border-top-color: $color;
        }
      }
    }

    // Disabled style
    &.disabled,
    &[disabled] {
      @include button-disabled;

      @each $name, $color in $button-palette {
        &.#{$name} {
          @include button-disabled($color, auto);
        }
      }
    }

    // Hollow style
    @if $button-fill != hollow {
      &.hollow {
        @include button-hollow;
        @include button-hollow-style;

        @each $name, $color in $button-palette {
          &.#{$name} {
            @include button-hollow-style($color);
          }
        }
      }
    }

    // Clear style
    @if $button-fill != clear {
      &.clear {
        @include button-hollow;
        @include button-hollow-style;

        &, &:hover, &:focus {
          &, &.disabled, &[disabled] {
            border-color: transparent;
          }
        }

        @each $name, $color in $button-palette {
          &.#{$name} {
            //@include button-hollow-style($color);
            $color-hover: scale-color($color, $alpha: $button-hollow-hover-lightness);

            &:before {
              background-color: $color;
            }

            &:hover,
            &:focus {
                color: $color-hover;
                border-color: transparent;
                &:before {
                  background-color: $color-hover;
                }
            }
          }
        }
      }
    }

  }
  a.button {
    &:hover,
    &:focus {
      text-decoration: none;
    }
  }
}
